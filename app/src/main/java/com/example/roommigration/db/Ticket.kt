package com.example.roommigration.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface TicketDao {
    @Query("SELECT * FROM Ticket")
    fun tickets(): Flow<List<Ticket>>

    @Query("SELECT * FROM Ticket")
    suspend fun ticketsList(): List<Ticket>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(ticket: Ticket)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(tickets: List<Ticket>)

    @Query("DELETE FROM Ticket WHERE name = :ticketName")
    suspend fun delete(ticketName: String)
}

@Entity
class Ticket(
    @PrimaryKey val name: String,
    val description:String? = null
)