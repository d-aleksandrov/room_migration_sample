package com.example.roommigration.db

import com.example.roommigration.App
import kotlinx.coroutines.flow.Flow

class TicketsRepository {
    private val dao = App.room.tickets()

    fun tickets(): Flow<List<Ticket>> = dao.tickets()

    suspend fun initialize() {
        if (dao.ticketsList().isEmpty())
            dao.insert(
                listOf(
                    Ticket("one"),
                    Ticket("two"),
                    Ticket("three")
                )
            )
    }
}