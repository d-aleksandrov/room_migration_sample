package com.example.roommigration.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(
    entities = [Ticket::class],
    version = 2,
    exportSchema = false
)
abstract class MainRoom : RoomDatabase() {
    abstract fun tickets(): TicketDao

    companion object{
        val migration_1_2 = object :Migration(1, 2){
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL(
                    "ALTER TABLE Ticket ADD COLUMN description TEXT DEFAULT null"
                )
            }
        }
    }
}