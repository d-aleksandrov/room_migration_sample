package com.example.roommigration

import android.app.Application
import androidx.room.Room
import com.example.roommigration.db.MainRoom

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        room = Room
            .databaseBuilder(applicationContext, MainRoom::class.java, "database")
            .fallbackToDestructiveMigration()
            .addMigrations(MainRoom.migration_1_2)
            .build()
    }

    companion object {
        lateinit var room: MainRoom
    }
}