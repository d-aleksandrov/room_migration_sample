package com.example.roommigration.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.example.roommigration.R
import com.example.roommigration.db.Ticket

class MainFragment : Fragment() {
    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.main_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.tickets.observe(viewLifecycleOwner, ::updateTickets)
    }

    private fun updateTickets(tickets: List<Ticket>?) {
        tickets ?: return

        val adapter = TicketsAdapter(tickets)
        val recycler = requireView().findViewById<RecyclerView>(R.id.recycler)
        recycler.adapter = adapter
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}