package com.example.roommigration.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.roommigration.R
import com.example.roommigration.db.Ticket

class TicketsAdapter(
    private val tickets: List<Ticket>
) : RecyclerView.Adapter<TicketsAdapter.TicketsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_ticket, parent, false)
        return TicketsViewHolder(view)
    }

    override fun getItemCount(): Int = tickets.size

    override fun onBindViewHolder(holder: TicketsViewHolder, position: Int) {
        val textView = holder.itemView.findViewById<AppCompatTextView>(R.id.ticketName)
        textView.text = tickets.getOrNull(position)?.name
    }

    class TicketsViewHolder(view: View) : RecyclerView.ViewHolder(view)
}