package com.example.roommigration.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.roommigration.App
import com.example.roommigration.db.Ticket
import com.example.roommigration.db.TicketsRepository
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val repository = TicketsRepository()

    val tickets: LiveData<List<Ticket>> =
        repository.tickets().asLiveData(viewModelScope.coroutineContext)

    init {
        viewModelScope.launch { repository.initialize() }
    }
}